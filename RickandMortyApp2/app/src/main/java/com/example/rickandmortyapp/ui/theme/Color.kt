package com.example.rickandmortyapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val pers1 = Color(0xFF056D77)
val black = Color(0xFF000000)
val pers2 = Color(0xFF033B41)
val orange = Color(0xFFF5501D)
val green = Color(0xFFa8cd3f)