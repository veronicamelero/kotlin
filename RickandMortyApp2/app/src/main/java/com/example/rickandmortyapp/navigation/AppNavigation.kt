package com.example.rickandmortyapp.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.rickandmortyapp.ViewModels.PersonajesViewModel
import com.example.rickandmortyapp.screens.*

@Composable
fun AppNavigation(ViewModel: PersonajesViewModel) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = AppScreens.SplahScreen.route)
    {
        composable(route = AppScreens.SplahScreen.route) {
            SplashScreen(navController)
        }

        composable(route = AppScreens.Inicio.route) {
            Inicio(navController)
        }

        composable(route = AppScreens.InfoScreen.route) {
            InfoScreen(navController)
        }

        composable(route = AppScreens.PersonajesScreen.route) {
            PersonajesScreen(navController, viewModel = ViewModel)
        }

        composable(route = AppScreens.PersonajeDetailScreen.route)
        {
            PersonajeDetailScreen(
                navController = navController,
                viewModel = ViewModel
            )
        }

        composable(route = AppScreens.PersonajesDatabase.route) {
            PersonajesDatabase(navController, viewModel = ViewModel)
        }

        composable(route = AppScreens.PersonajeDatabaseDetailScreen.route) {
            PersonajeDatabaseDetailScreen(navController = navController, viewModel = ViewModel)
        }
    }
}