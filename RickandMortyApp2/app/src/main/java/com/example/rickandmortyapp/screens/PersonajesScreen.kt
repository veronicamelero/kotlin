package com.example.rickandmortyapp.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Alignment.Companion.End
import androidx.compose.ui.Alignment.Companion.Start
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.rickandmortyapp.R
import com.example.rickandmortyapp.ViewModels.PersonajesViewModel
import com.example.rickandmortyapp.navigation.AppScreens
import com.example.rickandmortyapp.ui.theme.black
import com.example.rickandmortyapp.ui.theme.green
import com.example.rickandmortyapp.ui.theme.orange
import com.example.rickandmortyapp.ui.theme.pers1
import org.intellij.lang.annotations.JdkConstants.HorizontalAlignment

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun PersonajesScreen(navController: NavController, viewModel: PersonajesViewModel) {
    Scaffold(topBar = { topBar(navController = navController) },
        floatingActionButton = {
            FloatingActionButton(
                backgroundColor = pers1,
                onClick = { navController.navigate(AppScreens.Inicio.route) }) {
                Image(
                    painterResource(id = R.drawable.home),
                    contentDescription = "Volver al inicio"
                )
            }
        },
        floatingActionButtonPosition = FabPosition.Center,
        isFloatingActionButtonDocked = true,
        bottomBar = { bottomListBar(navController = navController, viewModel)},
        backgroundColor = black)
    {
        Divider(thickness = 2.dp, color = pers1, modifier = Modifier.padding(horizontal = 8.dp))
        ListadoPersonajes(navController = navController, viewModel = viewModel)
        Divider(thickness = 2.dp, color = pers1, modifier = Modifier.padding(horizontal = 24.dp))
    }

}

@Composable
fun topBar(navController: NavController)
{
    TopAppBar(
        backgroundColor = black,
        contentColor = green,

        modifier = Modifier
            .fillMaxWidth()
            .height(100.dp)

    ) {
        Icon(imageVector = Icons.Default.ArrowBack,
            contentDescription = "Arrow Back",

            modifier = Modifier
                .padding(end = 8.dp, start = 8.dp)
                .clickable {
                    navController.navigate(route = AppScreens.Inicio.route)

                })

        Image(
            painterResource(id = R.drawable.logomenu),
            contentDescription = "Logo de Rick and Morty",
            modifier = Modifier
                .size(300.dp)
                .padding(start = 32.dp, top = 8.dp, bottom = 8.dp)
        )

    }
}

@Composable
fun bottomListBar(navController: NavController, viewModel: PersonajesViewModel){
    BottomAppBar(
        // Defaults to null, that is, No cutout
        cutoutShape = MaterialTheme.shapes.small.copy(
            CornerSize(percent = 50)
        ),
        backgroundColor = black ,
        contentColor = green,
        elevation = 16.dp,
        modifier = Modifier.padding(top = 16.dp)

        ) {
        Row (verticalAlignment = Alignment.CenterVertically)
        {
            Column(
                horizontalAlignment = Alignment.Start,
                modifier = Modifier
                    .fillMaxWidth(0.5f)
            ) {
               Row() {
                   Icon(imageVector = Icons.Default.ArrowBack,
                       contentDescription = "Arrow Back",

                       modifier = Modifier
                           .padding(end = 8.dp, start = 8.dp)

                           .clickable {
                               if (viewModel.numPage.value!! >= 2) {
                                   viewModel.numPage.value = viewModel.numPage.value!! - 1
                                   navController.navigate(route = AppScreens.PersonajesScreen.route)
                               } else {

                               }
                           }
                   )

                   Text(
                       text = "Anterior",
                       color = pers1,
                       style = MaterialTheme.typography.button
                   )
               }
            }

            Column(
                horizontalAlignment = End,
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(end = 8.dp)
            ) {
                Row(
                ) {
                    Text(
                        text = "Siguiente",
                        color = pers1,
                        style = MaterialTheme.typography.button,
                    )

                    Icon(imageVector = Icons.Default.ArrowForward,
                        contentDescription = "Arrow Back",

                        modifier = Modifier
                            .padding(end = 8.dp, start = 8.dp)
                            .clickable {
                                if (viewModel.numPage.value!! < 42)
                                    viewModel.numPage.value = viewModel.numPage.value!! + 1
                                navController.navigate(route = AppScreens.PersonajesScreen.route)
                            }
                    )
                }
            }

        }
        }
}

@Composable
fun ListadoPersonajes(navController: NavController, viewModel: PersonajesViewModel){
    viewModel.cargarPersonajesApi()
    val state = viewModel.state

    if (state.isLoading) {
        Box(modifier = Modifier
            .background(black)
            .fillMaxSize(),
            contentAlignment = Alignment.Center,
        ) {
            CircularProgressIndicator()
        }
    }
    if (state.characters.isNotEmpty()) {
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .background(black)
                .padding(bottom = 8.dp)
        ) {
            items(state.characters) {
                Row(
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth()
                        .clickable {
                            viewModel.saveCharacter(it)
                            navController.navigate(route = AppScreens.PersonajeDetailScreen.route)
                        }
                ) {
                    AsyncImage(
                        model = it.image,
                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(20.dp)),
                        contentDescription = null
                    )

                    Spacer(modifier = Modifier.width(32.dp))

                    Text(
                        modifier = Modifier.align(CenterVertically),
                        text = it.name,
                        color = green,
                        style = MaterialTheme.typography.button
                    )
                }
                Divider(thickness = 0.5.dp, color = pers1,
                    modifier = Modifier
                        .padding(start = 24.dp, end = 24.dp)
                )
            }


        }
    }
}


