package com.example.rickandmortyapp.APIServices

data class Origin(
    val name: String,
    val url: String
)