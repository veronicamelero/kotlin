package com.example.rickandmortyapp.screens

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import com.example.rickandmortyapp.R
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.rickandmortyapp.APIServices.PersonajeLista
import com.example.rickandmortyapp.ViewModels.PersonajesViewModel
import com.example.rickandmortyapp.navigation.AppScreens
import com.example.rickandmortyapp.ui.theme.black
import com.example.rickandmortyapp.ui.theme.green
import com.example.rickandmortyapp.ui.theme.pers1

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable()
fun PersonajeDetailScreen(
    navController: NavController,
    viewModel: PersonajesViewModel
) {
    Scaffold(
        topBar = { topDetailBar(navController = navController) },
        floatingActionButton = {
            FloatingActionButton(
                backgroundColor = green,
                onClick = { navController.navigate(AppScreens.Inicio.route) }) {
                Image(
                    painterResource(id = R.drawable.home),
                    contentDescription = "Volver al inicio"
                )
            }
        },
        isFloatingActionButtonDocked = true,
        bottomBar = { bottomApiDetailBar(navController = navController)},
        backgroundColor = black)
    {
        Divider(thickness = 2.dp, color = pers1, modifier = Modifier.padding(horizontal = 8.dp))
        DetailScreen(navController = navController, viewModel = viewModel)
    }
}


@Composable
fun topDetailBar(navController: NavController){
    TopAppBar(

        backgroundColor = black,
        contentColor = green,

        modifier = Modifier
            .fillMaxWidth()
            .height(100.dp)

    ) {
        Icon(imageVector = Icons.Default.ArrowBack,
            contentDescription = "Arrow Back",

            modifier = Modifier
                .padding(end = 8.dp, start = 8.dp)
                .clickable {
                    navController.navigate(route = AppScreens.PersonajesScreen.route)
                })

        Image(
            painterResource(id = R.drawable.logomenu),
            contentDescription = "Logo de Rick and Morty",
            modifier = Modifier
                .size(300.dp)
                .padding(start = 32.dp, top = 8.dp, bottom = 8.dp)
        )
    }
}

@Composable
fun bottomApiDetailBar(navController: NavController){
    BottomAppBar(
        // Defaults to null, that is, No cutout
        cutoutShape = MaterialTheme.shapes.small.copy(
            CornerSize(percent = 50)
        ),
        backgroundColor = black ,
        elevation = 16.dp,

        ) {
        /* Bottom app bar content */
    }
}

@Composable
fun DetailScreen(navController: NavController, viewModel: PersonajesViewModel){
    val personajeSeleccionado: LiveData<PersonajeLista> = viewModel.selectedCharacter
    val mContext = LocalContext.current

    if(personajeSeleccionado != null){
        Card(
            elevation = 16.dp,
            shape = RoundedCornerShape(8.dp),

            backgroundColor = black,
            contentColor = green,
            modifier = Modifier
                .padding(top = 24.dp, bottom = 24.dp)
                .fillMaxWidth()
        ) {
            Column(
                horizontalAlignment = CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                AsyncImage(
                    model = personajeSeleccionado.value!!.image,
                    modifier = Modifier
                        .padding(16.dp)
                        .clip(shape = RoundedCornerShape(32.dp))
                        .align(CenterHorizontally)
                        .size(350.dp),
                    contentDescription = null
                )
               Column(
                   modifier = Modifier.padding(32.dp)
               ) {
                   Row(
                       horizontalArrangement = Arrangement.Start
                   ) {
                       Text(
                           text = "Nombre: ",
                           style = MaterialTheme.typography.body2
                       )
                       Text(
                           text = personajeSeleccionado.value!!.name ,
                           style = MaterialTheme.typography.body2
                       )
                   }


                   Row() {
                       Text(text = "Especie: ", style = MaterialTheme.typography.body2)
                       Text(text = personajeSeleccionado.value!!.species, style = MaterialTheme.typography.body2)
                   }

                   Row() {
                       Text(text = "Estado: ", style = MaterialTheme.typography.body2)
                       Text(text = personajeSeleccionado.value!!.status, style = MaterialTheme.typography.body2)
                   }

                   Row() {
                       Text(text = "Tipo: ", style = MaterialTheme.typography.body2)
                       Text(text = personajeSeleccionado.value!!.type, style = MaterialTheme.typography.body2)
                   }

                   Row() {
                       Text(text = "Género: ", style = MaterialTheme.typography.body2)
                       Text(text = personajeSeleccionado.value!!.gender, style = MaterialTheme.typography.body2)
                   }

                   Button(
                       onClick = {
                           val personaje = viewModel.selectedCharacter.value
                           if (personaje != null) {
                               viewModel.saveInDatabase(personaje)
                               viewModel.retrieveCharacters()
                               if (viewModel.success.value == true){
                                   mToast(mContext, "Personaje guardado correctamente")
                               } else {
                                   mToast(mContext, "No se ha podido guardar")
                               }
                           }
                       },
                       colors = ButtonDefaults.buttonColors(
                           backgroundColor = black
                       ),
                       border = BorderStroke(1.5.dp, pers1),
                       shape = RoundedCornerShape(40),
                       modifier = Modifier
                           .height(80.dp)
                           .width(200.dp)
                           .padding(top = 24.dp)
                           .align(CenterHorizontally)
                   ) {

                       Text(
                           text = "Guardar",
                           style = MaterialTheme.typography.button,
                           color = green,
                           modifier = Modifier
                               .align(Alignment.CenterVertically)
                               .padding(5.dp)
                       )
                   }

               }

            }
        }
    }
}

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}