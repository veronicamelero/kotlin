package com.example.rickandmortyapp.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.*

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.rickandmortyapp.R
import com.example.rickandmortyapp.navigation.AppScreens
import com.example.rickandmortyapp.ui.theme.black
import com.example.rickandmortyapp.ui.theme.green
import com.example.rickandmortyapp.ui.theme.pers1

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun InfoScreen(navController: NavController)
{
    Scaffold(
        topBar = { topInfoBar(navController) },
        floatingActionButton = {
            FloatingActionButton(
                backgroundColor = pers1,
                onClick = { navController.navigate(AppScreens.Inicio.route) }) {
                Image(
                    painterResource(id = R.drawable.home),
                    contentDescription = "Volver al inicio"
                )
            }
        },
        isFloatingActionButtonDocked = true,
        bottomBar = { bottomInfoBar(navController = navController)},
    )
    {
        Divider(thickness = 1.5.dp, color = pers1, modifier = Modifier.padding(horizontal = 16.dp))
        InfoScreenDesign(navController)
    }

}

@Composable
fun topInfoBar(navController: NavController){
    TopAppBar(
        backgroundColor = black,
        contentColor = green,

        modifier = Modifier
            .fillMaxWidth()
            .height(100.dp)

    ) {
        Icon(imageVector = Icons.Default.ArrowBack,
            contentDescription = "Arrow Back",

            modifier = Modifier
                .padding(end = 8.dp, start = 8.dp)
                .clickable {
                    navController.navigate(route = AppScreens.Inicio.route)

                })

        Image(
            painterResource(id = R.drawable.logomenu),
            contentDescription = "Logo de Rick and Morty",
            modifier = Modifier
                .size(300.dp)
                .padding(start = 32.dp, top = 8.dp, bottom = 8.dp)
        )

    }
}

@Composable
fun bottomInfoBar(navController: NavController){
    BottomAppBar(
        // Defaults to null, that is, No cutout
        cutoutShape = MaterialTheme.shapes.small.copy(
            CornerSize(percent = 50)
        ),
        backgroundColor = black ,
        elevation = 16.dp,

        ) {
        /* Bottom app bar content */
    }
}

@Composable
fun InfoScreenDesign(navController: NavController) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(black)
            .verticalScroll(
                rememberScrollState()
            )
    ) {

        Image(
            painterResource(id = R.drawable.rickfamily),
            contentDescription = "Imagen de Rick and Morty",
            modifier = Modifier
                .padding(bottom = 16.dp)
                .fillMaxWidth()
                .align(CenterHorizontally)
        )

        Text(
            text = "Rick y Morty es una serie de televisión americana de animación para adultos que " +
                    "sigue las desventuras de un científico, Rick Sanchez, y su fácilmente " +
                    "influenciable nieto, Morty, quienes pasan el tiempo entre la vida doméstica y " +
                    "los viajes espaciales, temporales e intergalácticos.\n \n \n" +
                    "Rick Sánchez es un ejemplo del típico \"científico loco\". Es un genio, pero es" +
                    " irresponsable, alcohólico, egoísta, un poco depresivo y con poca cordura.\nRick" +
                    " por diferentes razones termina mudándose a la casa de su hija Beth y en ese " +
                    "momento se encuentra con su nieto Morty; un chico de 14 años de edad, tímido y " +
                    "no muy listo. Al juntarse con su nieto, Rick y Morty viven una variedad de " +
                    "aventuras a lo largo del cosmos y universos paralelos. \nY es mediante tantas " +
                    "vivencias y reflexiones que Rick busca que su nieto Morty no acabe como su " +
                    "padre, Jerry, un hombre muy poco exitoso que a pesar de tener buenas intenciones" +
                    " resulta ser bastante inútil en muchas ocasiones y depende mucho de su esposa," +
                    " Beth, hija de Rick.\n" +
                    "\n" +
                    "A pesar de estar muy apegados, Rick y su nieto experimentan momentos en los que" +
                    " Summer, hermana de Morty, se une en ocasiones a las pintorescas aventuras " +
                    "provocadas por Rick.",
            style = MaterialTheme.typography.body2,
            color = green,
            modifier = Modifier
                .align(CenterHorizontally)
                .padding(horizontal = 32.dp)

        )

        Image(
            painterResource(id = R.drawable.rickandmortypeineta),
            contentDescription = "Imagen de Rick and Morty",
            modifier = Modifier
                .padding(bottom = 16.dp)
                .fillMaxWidth()
                .align(CenterHorizontally)
                .size(250.dp)
        )
    }
}
