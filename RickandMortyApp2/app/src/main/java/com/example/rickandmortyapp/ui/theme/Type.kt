package com.example.rickandmortyapp.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.rickandmortyapp.R

val openSans = FontFamily(
    Font(R.font.open_sans_light, FontWeight.Light),
    Font(R.font.open_sans_bold, FontWeight.Bold),
    Font(R.font.open_sans_regular, FontWeight.Normal),
    Font(R.font.open_sans_medium, FontWeight.Medium)
)

val grandstarter = FontFamily(
    Font(R.font.grandstander_light, FontWeight.Light),
    Font(R.font.grandstander_regular, FontWeight.Normal),
    Font(R.font.grandstander_medium, FontWeight.SemiBold),
    Font(R.font.grandstander_bold, FontWeight.Bold),
    Font(R.font.grandstander_semibolditalic, FontWeight.SemiBold)
)



// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = grandstarter   ,
        fontWeight = FontWeight.Light,
        fontSize = 20.sp
    ),

    body2 = TextStyle(
        fontFamily = grandstarter,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),

    h1 = TextStyle(
        fontFamily = grandstarter,
        fontWeight = FontWeight.Bold,
        fontSize = 24.sp
    ),

    button = TextStyle(
        fontFamily = grandstarter,
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp
    ),

    h3 = TextStyle(
        fontFamily = grandstarter,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    )




/*,
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
)