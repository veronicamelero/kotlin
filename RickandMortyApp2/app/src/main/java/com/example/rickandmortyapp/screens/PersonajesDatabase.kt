package com.example.rickandmortyapp.screens

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.rickandmortyapp.APIServices.PersonajeLista
import com.example.rickandmortyapp.R
import com.example.rickandmortyapp.ViewModels.PersonajesViewModel
import com.example.rickandmortyapp.navigation.AppScreens
import com.example.rickandmortyapp.ui.theme.black
import com.example.rickandmortyapp.ui.theme.green
import com.example.rickandmortyapp.ui.theme.pers1
import com.google.firebase.firestore.FirebaseFirestore

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable()
fun PersonajesDatabase(
    navController: NavController,
    viewModel: PersonajesViewModel
){
    Scaffold(
        topBar = { topDatabaseBar(navController = navController) },
        floatingActionButton = {
            FloatingActionButton(
                backgroundColor = pers1,
                onClick = { navController.navigate(AppScreens.Inicio.route) }) {
                Image(
                    painterResource(id = R.drawable.home),
                    contentDescription = "Volver al inicio"
                )
            }
        },
        isFloatingActionButtonDocked = true,
        bottomBar = { bottomDbBar(navController = navController)},
        backgroundColor = black, contentColor = green)
    {
        Divider(thickness = 2.dp, color = pers1, modifier = Modifier.padding(horizontal = 8.dp))
        viewModel.retrieveCharacters()
        mostrarCharacters(viewModel, navController)
    }
}

@Composable
fun bottomDbBar(navController: NavController) {
    BottomAppBar(
        // Defaults to null, that is, No cutout
        cutoutShape = MaterialTheme.shapes.small.copy(
            CornerSize(percent = 50)
        ),
        backgroundColor = black ,
        elevation = 16.dp,

        ) {
        /* Bottom app bar content */
    }
}


@Composable
fun topDatabaseBar(navController: NavController){
        TopAppBar(
            backgroundColor = black,
            contentColor = green,

            modifier = Modifier
                .fillMaxWidth()
                .height(100.dp)

        ) {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Arrow Back",

                modifier = Modifier
                    .padding(end = 8.dp, start = 8.dp)
                    .clickable {
                        navController.navigate(route = AppScreens.Inicio.route)
                    })

            Image(
                painterResource(id = R.drawable.logomenu),
                contentDescription = "Logo de Rick and Morty",
                modifier = Modifier
                    .size(300.dp)
                    .padding(start = 32.dp, top = 8.dp, bottom = 8.dp)
            )
        }
    }


@Composable
fun mostrarCharacters(viewModel: PersonajesViewModel, navController: NavController) {
    var charactersDb =viewModel.personajesDb

    if (charactersDb.isNotEmpty()) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(black)
                .padding(bottom = 8.dp)
        ) {
            items(charactersDb) {
                //println(it)
                Row(
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth()
                        .clickable {
                            viewModel.saveCharacter(it)
                            navController.navigate(route = AppScreens.PersonajeDatabaseDetailScreen.route)
                        }
                ) {
                    AsyncImage(
                        model = it.image,
                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(20.dp)),
                        contentDescription = null
                    )

                    Spacer(modifier = Modifier.width(32.dp))

                    Text(
                        modifier = Modifier.align(Alignment.CenterVertically),
                        text = it.name,
                        color = green,
                        style = MaterialTheme.typography.button
                    )
                }
                Divider(thickness = 0.5.dp, color = pers1,
                    modifier = Modifier
                        .padding(start = 24.dp, end = 24.dp)
                )
            }
        }
    } else {
        Text("No hay personajes")
    }
}