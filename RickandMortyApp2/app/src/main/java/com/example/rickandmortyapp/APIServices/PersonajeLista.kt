package com.example.rickandmortyapp.APIServices

import android.os.Parcel

data class PersonajeLista(
    val image: String,
    val name: String,
    val species: String,
    val status: String,
    val type: String,
    val gender: String
): java.io.Serializable {



}
