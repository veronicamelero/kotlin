package com.example.rickandmortyapp.APIServices

class RickAndMortyRepository (
    private val api: API
){
    //Método que extrae todos los personajes de la lista y controla si hay algun fallo de conexion con la API
    suspend fun getPersonajes(page: Int): Result<List<PersonajeLista>>{
        return try{
            val response = api.getPersonajes(page).personajes
            val characters = response.map { convert(it) }
            Result.success(characters)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

    //convierte los personajes en tipo personajeLista que solo tiene los atributos que voy a querer utilizar en las pantallas
    private fun convert(personaje: Personaje): PersonajeLista {
        return PersonajeLista(
            name = personaje.name,
            image = personaje.image,
            species = personaje.species,
            status = personaje.status,
            type = personaje.type,
            gender = personaje.gender
        )
    }

}