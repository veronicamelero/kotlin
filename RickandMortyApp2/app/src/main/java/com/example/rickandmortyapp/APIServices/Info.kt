package com.example.rickandmortyapp.APIServices


data class Info(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: Any
)