package com.example.rickandmortyapp.APIServices


import com.google.gson.annotations.SerializedName

data class PersonajesResponse(
    @SerializedName("info")
    val info: Info,
    @SerializedName("results")
    val personajes: List<Personaje>
)