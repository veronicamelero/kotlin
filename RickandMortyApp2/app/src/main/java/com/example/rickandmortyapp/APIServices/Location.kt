package com.example.rickandmortyapp.APIServices


data class Location(
    val name: String,
    val url: String
)