package com.example.rickandmortyapp.screens

import android.window.SplashScreen
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.rickandmortyapp.R
import com.example.rickandmortyapp.navigation.AppScreens
import com.example.rickandmortyapp.ui.theme.black
import com.example.rickandmortyapp.ui.theme.green
import kotlinx.coroutines.delay

@Composable
fun SplashScreen(navController: NavController) {
    LaunchedEffect(key1 = true) {
        delay(5000)
        navController.popBackStack()
        navController.navigate(route = AppScreens.Inicio.route)
    }
    Splash(navController)
}

@Composable
fun Splash(navController: NavController){
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(black),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center

    ) {

        Image(
            painterResource(id = R.drawable.logoprincipal),
            contentDescription = "Logo de Rick and Morty",
            modifier = Modifier
                .padding(top = 16.dp, bottom = 24.dp)
                .size(500.dp)
                .align(Alignment.CenterHorizontally)
        )

        Text(
            text = "¡Bienvenidos¡",
            style = MaterialTheme.typography.h1,
            color = green,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 24.dp)

        )
    }
}
