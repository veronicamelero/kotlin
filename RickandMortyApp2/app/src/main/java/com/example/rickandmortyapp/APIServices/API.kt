package com.example.rickandmortyapp.APIServices

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface API {
    //Creamos instancia de Retrofit

companion object {
    val instance = Retrofit.Builder().baseUrl("https://rickandmortyapi.com/api/").addConverterFactory(GsonConverterFactory.create()).client(
        OkHttpClient.Builder().build()
    ).build().create(API::class.java)
}
    @GET("character/")
    suspend fun getPersonajes(@Query("page") page: Int): PersonajesResponse

}