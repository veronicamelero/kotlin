package com.example.rickandmortyapp.navigation

sealed class AppScreens (val route: String) {
    object SplahScreen: AppScreens("SplashScreen")
    object Inicio: AppScreens("Inicio")
    object InfoScreen: AppScreens("InfoScreen")
    object PersonajesScreen: AppScreens("PersonajesScreen")
    object PersonajeDetailScreen: AppScreens("PersonajeDetailScreen")
    object PersonajesDatabase: AppScreens("PersonajesDatabase")
    object PersonajeDatabaseDetailScreen: AppScreens ("PersonajeDatabaseDetailScreen")
}