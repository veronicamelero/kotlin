package com.example.rickandmortyapp.screens

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.rickandmortyapp.R
import com.example.rickandmortyapp.navigation.AppScreens
import com.example.rickandmortyapp.ui.theme.*

@Composable
fun Inicio(navController: NavHostController) {
    InicioBodyContent(navController = navController)
}


@Composable
fun InicioBodyContent(navController: NavHostController) {

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(black)

    ) {

        Image(
            painterResource(id = R.drawable.logoprincipal),
            contentDescription = "Logo de Rick and Morty",
            modifier = Modifier
                .padding(top = 16.dp, bottom = 32.dp)
                .size(500.dp)
                .align(CenterHorizontally)
        )

        Spacer(modifier = Modifier.size(16.dp))

        Button(
            onClick = {
                navController.navigate(AppScreens.InfoScreen.route)
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = black
            ),
            border = BorderStroke(1.5.dp, pers1),
            shape = RoundedCornerShape(40),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .padding(top = 8.dp)
                .align(CenterHorizontally)
        ) {

            Text(
                text = "Información",
                style = MaterialTheme.typography.button,
                color = green,
                modifier = Modifier
                    .align(CenterVertically)
            )
        }

        Spacer(modifier = Modifier.size(24.dp))

        Button(
            onClick = {
                navController.navigate(AppScreens.PersonajesScreen.route)
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = black
            ),
            border = BorderStroke(1.5.dp, pers1),
            shape = RoundedCornerShape(40),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .padding(top = 8.dp)
                .align(CenterHorizontally)
        ) {

            Text(
                text = "Ver personajes",
                style = MaterialTheme.typography.button,
                color = green,
                modifier = Modifier
                    .align(CenterVertically)

            )
        }

        Spacer(modifier = Modifier.size(24.dp))

        Button(
            onClick = {
                navController.navigate(AppScreens.PersonajesDatabase.route)
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = black
            ),
            border = BorderStroke(1.5.dp, pers1),
            shape = RoundedCornerShape(40),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .padding(top = 8.dp)
                .align(CenterHorizontally)
        ) {

            Text(
                text = "Mis personajes",
                style = MaterialTheme.typography.button,
                color = green,
                modifier = Modifier
                    .align(CenterVertically)
            )
        }


    }



}
