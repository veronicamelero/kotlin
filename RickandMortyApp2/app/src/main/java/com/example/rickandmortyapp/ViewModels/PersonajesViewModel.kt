package com.example.rickandmortyapp.ViewModels

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rickandmortyapp.APIServices.API
import com.example.rickandmortyapp.APIServices.MainState
import com.example.rickandmortyapp.APIServices.PersonajeLista
import com.example.rickandmortyapp.APIServices.RickAndMortyRepository
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PersonajesViewModel(private val repository: RickAndMortyRepository = RickAndMortyRepository(
    API.instance,)
) :ViewModel()
{
    var state by mutableStateOf(MainState())
    private set

    private val _selectedCharacter = MutableLiveData<PersonajeLista>()
    val selectedCharacter : LiveData<PersonajeLista> = _selectedCharacter

    private val _charactersDb = MutableLiveData<List<PersonajeLista>>()
    //val charactersDb: LiveData<List<PersonajeLista>> = _charactersDb


    var success = MutableLiveData<Boolean>()

    //Esta variable controla el numero de la pagina de personajes que cargamos de la API. Se
    // inicializa en valor 1 para cargar la primera pagina y este valor se va incrementando con un boton
    // que hay en la pagina de la lista de personajes.
    val numPage = MutableLiveData<Int>(1)

fun cargarPersonajesApi() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            repository.getPersonajes(page = numPage.value!!).onSuccess {

                state = state.copy(
                    characters = it
                )

            }.onFailure {

            }
            state = state.copy(isLoading = false)
        }
}
    fun saveCharacter(character: PersonajeLista) {
        _selectedCharacter.value = character
    }

    fun saveInDatabase(character: PersonajeLista) {
        val db = FirebaseFirestore.getInstance()
        val nombre_coleccion = "personajes"
        val dato = hashMapOf<String, String>()

        dato["name"] = character.name
        dato["gender"] = character.gender
        dato["image"] = character.image
        dato["type"] = character.type
        dato["status"] = character.status
        dato["species"] = character.species

        viewModelScope.launch {
            db.collection(nombre_coleccion).add(dato)
                .addOnSuccessListener {
                    success.postValue(true)
                }
                .addOnFailureListener {
                    success.postValue(false)
                }
        }
    }


    var personajesDb: MutableList<PersonajeLista> by mutableStateOf(mutableListOf())

    fun retrieveCharacters(): MutableList<PersonajeLista> {
        viewModelScope.launch {

            val db = FirebaseFirestore.getInstance()
            val nombre_coleccion = "personajes"
            db.collection(nombre_coleccion)
                .get()
                .addOnSuccessListener { result ->
                    val list = mutableListOf<PersonajeLista>()
                    for (document in result) {
                        val image = document.get("image").toString()
                        val name = document.get("name").toString()
                        val species = document.get("species").toString()
                        val status = document.get("status").toString()
                        val type = document.get("type").toString()
                        val gender = document.get("gender").toString()
                        val personaje = PersonajeLista(image, name, species, status, type, gender)
                        list.add(personaje)
                    }
                    personajesDb = list
                }
                .addOnFailureListener { exception ->
                    println("Error getting documents.")
                }
        }
        return personajesDb
    }

    fun deleteCharacterFromDatabase(character: PersonajeLista) {
        val db = FirebaseFirestore.getInstance()
        val nombre_coleccion = "personajes"
        db.collection(nombre_coleccion).whereEqualTo("name", character.name).get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val id = document.id
                    db.collection(nombre_coleccion).document(id).delete()
                }
            }
        //retrieveCharacters()
    }

}