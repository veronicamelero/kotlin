package com.example.rickandmortyapp.APIServices

data class MainState(
    val isLoading: Boolean = false,
    val characters: List<PersonajeLista> = emptyList()
)
