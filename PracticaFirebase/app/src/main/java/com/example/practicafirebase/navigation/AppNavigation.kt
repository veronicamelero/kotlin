package com.example.practicafirebase.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.practicafirebase.screens.*

@Composable
fun AppNavigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = AppScreens.Inicio.route)
    {
        composable(route = AppScreens.Inicio.route) {Inicio(navController)}
        composable(route = AppScreens.Create.route) {Create(navController)}
        composable(route = AppScreens.Read.route) {Read(navController)}
        composable(route = AppScreens.Update.route) {Update(navController)}
        composable(route = AppScreens.Delete.route) {Delete(navController)}
        composable(route = AppScreens.ShowContacts.route) {ShowContacts(navController)}
    }

}