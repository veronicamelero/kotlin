package com.example.practicafirebase.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.practicafirebase.R
import com.example.practicafirebase.navigation.AppScreens
import com.example.practicafirebase.ui.theme.*
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun Update(navController: NavHostController) {
    Scaffold(topBar = {
        TopAppBar(
            backgroundColor = Purple500
        ) {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Arrow Back",
                modifier = Modifier
                    .padding(end = 8.dp)
                    .align(Alignment.CenterVertically)
                    .clickable {
                        navController.navigate(route = AppScreens.Inicio.route)

                    })
            Text(
                text = "Pantalla Inicio",
                style = MaterialTheme.typography.h2,
                color = Purple200
            )
        }

    }) {
        UpdateBodyContent()
    }
}

@Composable
fun UpdateBodyContent() {
    val mContext = LocalContext.current
    val db = FirebaseFirestore.getInstance()

    val nombreColeccion = "Contactos"

    var Nombre by remember { mutableStateOf("") }
    var Telefono by remember { mutableStateOf("") }
    var Email by remember { mutableStateOf("") }


    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(Purple200)
            .padding(24.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        Image(
            painter = painterResource(id = R.drawable.agendapp1),
            contentDescription = "LogoAgenda",
            modifier = Modifier
                .padding(bottom = 24.dp)
                .size(width = 300.dp, height = 150.dp)
        )

        Text(
            text = "Modificar Contacto",
            style = MaterialTheme.typography.h2,
            color = Purple700
        )

        Spacer(modifier = Modifier.size(16.dp))

        var todosLosCamposSonValidos = false

        OutlinedTextField(
            value = Nombre,
            onValueChange = { Nombre = it
            },
            label = {
                Text(
                    "Introduce el nombre",
                    color = Purple700,
                    style = MaterialTheme.typography.h3
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Purple700,
                unfocusedBorderColor = Purple500
            ),
            modifier = Modifier.fillMaxWidth(),
            singleLine = true,
        )

        Spacer(modifier = Modifier.size(24.dp))

        OutlinedTextField(
            value = Telefono,
            onValueChange = { Telefono = it
            },
            label = {
                Text(
                    "Introduce el teléfono",
                    color = Purple700,
                    style = MaterialTheme.typography.h3
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Purple700,
                unfocusedBorderColor = Purple500
            ),
            modifier = Modifier.fillMaxWidth(),
            singleLine = true,
        )

        Spacer(modifier = Modifier.size(24.dp))

        OutlinedTextField(
            value = Email,
            onValueChange = { Email = it
            },
            label = {
                Text(
                    "Introduce el mail",
                    color = Purple700,
                    style = MaterialTheme.typography.h3
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Purple700,
                unfocusedBorderColor = Purple500
            ),
            modifier = Modifier.fillMaxWidth(),
            singleLine = true,
        )

        Spacer(modifier = Modifier.size(24.dp))

        val dato = hashMapOf<String, String>()

        if (Nombre.isNotBlank() && comprobarEmail(Email) && comprobarTel(Telefono)) {
            todosLosCamposSonValidos = true
        }

        Button(
            onClick = {
                // Se comprueban los datos antes de guardarlos en la BBDD. Si hay algun dato incorrecto no se
                // guardaran y saltará un mensaje para avisar que hay algo mal
                if (todosLosCamposSonValidos) {
                    dato["Nombre"] = Nombre
                    dato["Telefono"] = Telefono
                    dato["Email"] = Email

                    db.collection(nombreColeccion)
                        .document(Nombre)
                        .set(dato)
                        .addOnSuccessListener {
                            mToast(mContext, "Contacto actualizado")
                            Nombre = ""
                            Email = ""
                            Telefono = ""
                        }
                        .addOnFailureListener {
                            mToast(mContext, "No se ha podido actualizar")
                            Nombre = ""
                            Email = ""
                            Telefono = ""
                        }
                } else {
                    mToast(mContext, "Algún dato es incorrecto")
                }
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Purple500
            ),
            border = BorderStroke(2.dp, Purple300),
            shape = RoundedCornerShape(40),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .padding(vertical = 8.dp)
        ) {
            Text(
                text = "Guardar",
                style = MaterialTheme.typography.button,
                color = White,
                modifier = Modifier.padding(start = 10.dp)
            )
        }

        Spacer(modifier = Modifier.size(5.dp))
    }
}

//Chequea que el email cumpla con el patron estándar de un mail
private fun comprobarEmail(mail: String):Boolean{
    val emailRegex = Regex(pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")
    return mail.isNotBlank() && emailRegex.matches(mail)
}

//Chequea que el telefono tenga 9 caracteres y que todos sean numeros
private fun comprobarTel(tel: String): Boolean {
    val telefonoRegex = Regex("[0-9]{9}")
    return tel.isNotBlank() && telefonoRegex.matches(tel)
}

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}

