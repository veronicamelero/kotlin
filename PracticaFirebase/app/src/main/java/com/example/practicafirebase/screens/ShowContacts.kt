package com.example.practicafirebase.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.practicafirebase.R
import com.example.practicafirebase.navigation.AppScreens
import com.example.practicafirebase.ui.theme.*
import com.google.firebase.firestore.FirebaseFirestore


@Composable
fun ShowContacts(navController: NavHostController) {
    Scaffold(topBar = {
        TopAppBar(
            backgroundColor = Purple500
        ) {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Arrow Back",
                modifier = Modifier
                    .padding(end = 8.dp)
                    .align(Alignment.CenterVertically)
                    .clickable {
                        navController.navigate(route = AppScreens.Inicio.route)

                    })
            Text(
                text = "Pantalla Inicio",
                style = MaterialTheme.typography.h2,
                color = Purple200
            )
        }

    }) {
        ShowContactsBodyContent()
    }
}

@Composable
private fun ShowContactsBodyContent() {

    val mContext = LocalContext.current

    val db = FirebaseFirestore.getInstance()

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(Purple200)
            .padding(top = 10.dp)
            .padding(start = 10.dp)
            .padding(end = 10.dp)
    ) {

        Image(
            painter = painterResource(id = R.drawable.agendapp1),
            contentDescription = "LogoAgenda",
            modifier = Modifier
                .padding(bottom = 24.dp)
                .size(width = 300.dp, height = 150.dp)
        )

        Text(
            text = "Ver todos los contactos",
            style = MaterialTheme.typography.h2,
            color = Purple700
        )

        Spacer(modifier = Modifier.size(16.dp))

        //DECLARAMOS LA VARIABLE QUE VA A RECOGER LOS DATOS DE LA CONSULTA CON EL ESTADO REMEMBER

        var datos by remember { mutableStateOf("") }

        Button(
            onClick = {
                // VACIAMOS VARIABLE AL DAR AL BOTON
                datos = ""

                // HACEMOS LA CONSULTA A LA COLECCION CON GET
                db.collection("Contactos")
                    .get()

                    //SI SE CONECTA CORRECTAMENTE
                    // RECORRO TODOS LOS DATOS ENCONTRADOS EN LA COLECCIÓN Y LOS ALMACENO EN DATOS
                    .addOnSuccessListener { resultado ->
                        for (encontrado in resultado) {
                            datos += "Nombre: ${encontrado.id}\nTelefono: ${encontrado.data.get("Telefono")}\nEmail: ${
                                encontrado.data.get(
                                    "Email")}\n\n"
                            //Log.i("DATOS:", datos)
                        }

                        if (datos.isEmpty()) {
                            mToast(mContext, "No existen datos")
                        }
                    }
                    //SI NO CONECTA CORRECTAMENTE
                    .addOnFailureListener { resultado ->
                        mToast(mContext, "La conexión a FireStore no se ha podido completar")
                    }
            },

            colors = ButtonDefaults.buttonColors(
                backgroundColor = Purple500
            ),
            border = BorderStroke(2.dp, Purple300),
            shape = RoundedCornerShape(40),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .padding(vertical = 8.dp)
        )
        {

            Text(
                text = "Cargar Datos",
                style = MaterialTheme.typography.button,
                color = White,
                modifier = Modifier.padding(start = 10.dp)
            )
        }

        Spacer(modifier = Modifier.size(16.dp))

        if (datos.isNotEmpty()) {
            MostrarDatos(datos)
        }

    }
}

@Composable
private fun MostrarDatos(datos: String) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp),
        elevation = 12.dp,
        shape = MaterialTheme.shapes.medium,
        backgroundColor = Purple100,
        border = BorderStroke(1.5.dp, Purple700)
    ) {
        Text(
            text = datos,
            style = MaterialTheme.typography.button,
            color = Purple700,
            modifier = Modifier.padding(top = 10.dp, start = 10.dp)
        )
    }
}

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}
