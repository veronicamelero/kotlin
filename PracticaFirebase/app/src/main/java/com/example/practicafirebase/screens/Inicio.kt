package com.example.practicafirebase.screens

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.*
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.practicafirebase.R
import com.example.practicafirebase.navigation.AppScreens
import com.example.practicafirebase.ui.theme.*

@Composable
fun Inicio(navController: NavHostController) {
    Scaffold (
        modifier = Modifier.fillMaxSize()
            ){

        InicioBodyContent(navController)

    }
}

@Composable
fun InicioBodyContent(navController: NavHostController) {


    Card(
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .fillMaxSize()

    ) {

        Column(
            horizontalAlignment = CenterHorizontally,
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .background(Purple200)

        ) {

            Image(
                painter = painterResource(id = R.drawable.agendapp1),
                contentDescription = "LogoAgendApp",
                modifier = Modifier
                    .padding(top = 24.dp, bottom = 24.dp)
                    .align(CenterHorizontally)
            )

            Button(
                onClick = {
                    navController.navigate(route = AppScreens.Read.route)

                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Purple500
                ),
                border = BorderStroke(2.dp, Purple300),
                shape = RoundedCornerShape(40),

                modifier = Modifier
                    .height(60.dp)
                    .width(220.dp)
                    .padding(vertical = 8.dp)

            ) {
                Image(
                    painterResource(id = R.drawable.ic_search_person_blanco),
                    contentDescription = "Cart button icon",
                    modifier = Modifier.size(20.dp)
                )

                Text(
                    text = "Buscar contacto",
                    style = MaterialTheme.typography.button,
                    color = Color.White,
                    modifier = Modifier.padding(start = 5.dp)
                )
            }

            Spacer(modifier = Modifier.size(16.dp))

            Button(
                onClick = {
                    navController.navigate(route = AppScreens.Create.route)

                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Purple500
                ),
                border = BorderStroke(2.dp, Purple300),
                shape = RoundedCornerShape(40),
                modifier = Modifier
                    .height(60.dp)
                    .width(220.dp)
                    .padding(vertical = 8.dp)
            ) {
                Image(
                    painterResource(id = R.drawable.ic_add_person_blanco),
                    contentDescription = "Cart button icon",
                    modifier = Modifier.size(20.dp)
                )

                Text(
                    text = "Agregar Contacto",
                    style = MaterialTheme.typography.button,
                    color = White,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }

            Spacer(modifier = Modifier.size(16.dp))

            Button(
                onClick = {

                    navController.navigate(route = AppScreens.Update.route)
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Purple500
                ),
                border = BorderStroke(2.dp, Purple300),
                shape = RoundedCornerShape(40),
                modifier = Modifier
                    .height(60.dp)
                    .width(220.dp)
                    .padding(vertical = 8.dp)

            ) {
                Image(
                    painterResource(id = R.drawable.ic_refresh_person_blanco),
                    contentDescription = "Cart button icon",
                    modifier = Modifier.size(16.dp)
                )

                Text(
                    text = "Modificar contacto",
                    style = MaterialTheme.typography.button,
                    color = White,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }

            Spacer(modifier = Modifier.size(16.dp))

            Button(
                onClick = {

                    navController.navigate(route = AppScreens.Delete.route)
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Purple500
                ),
                border = BorderStroke(2.dp, Purple300),
                shape = RoundedCornerShape(40),
                modifier = Modifier
                    .height(60.dp)
                    .width(220.dp)
                    .padding(vertical = 8.dp)
            ) {
                Image(
                    painterResource(id = R.drawable.ic_remove_person_blanco),
                    contentDescription = "Cart button icon",
                    modifier = Modifier.size(20.dp)
                )

                Text(
                    text = "Borrar contacto",
                    style = MaterialTheme.typography.button,
                    color = White,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }

            Spacer(modifier = Modifier.size(16.dp))

            Button(
                onClick = {
                    navController.navigate(route = AppScreens.ShowContacts.route)

                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Purple500
                ),
                border = BorderStroke(2.dp, Purple300),
                shape = RoundedCornerShape(40),
                modifier = Modifier
                    .height(60.dp)
                    .width(220.dp)
                    .padding(vertical = 8.dp)

            ) {
                Image(
                    painterResource(id = R.drawable.ic_group_blanco),
                    contentDescription = "Cart button icon",
                    modifier = Modifier.size(20.dp)
                )

                Text(
                    text = "Mostrar contactos",
                    style = MaterialTheme.typography.button,
                    color = White,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }
        }

    }
}


@Preview(showBackground = true)
@Composable
fun Inicio() {

}