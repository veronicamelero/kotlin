package com.example.practicafirebase.ui.theme

import androidx.compose.ui.graphics.Color
val Purple100 = Color(0xFFC5C5F5)
val Purple200 = Color(0xFFa5a3f2)
val Purple300 = Color(0xFF9694E9)
val Purple500 = Color(0xFF3f48cc)
val Purple700 = Color(0xFF3A42BD)

val Teal100 = Color(0xFFB2DFDB)

val Teal300 = Color(0xFF4DB6AC)

val Teal700 = Color(0xFF018786)

val Teal900 = Color(0xFF004D40)

val TealA100 = Color(0xFFA7FFEB)
val TealA200 = Color(0xFF64FFDA)

val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
