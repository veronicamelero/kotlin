package com.example.practicafirebase.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.practicafirebase.R
import com.example.practicafirebase.navigation.AppScreens
import com.example.practicafirebase.ui.theme.*
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun Delete(navController: NavHostController) {
    Scaffold(topBar = {
        TopAppBar(
            backgroundColor = Purple500
        ) {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Arrow Back",
                modifier = Modifier
                    .padding(end = 8.dp)
                    .align(Alignment.CenterVertically)
                    .clickable {
                        navController.navigate(route = AppScreens.Inicio.route)

                    })
            Text(
                text = "Pantalla Inicio",
                style = MaterialTheme.typography.h2,
                color = Purple200
            )
        }

    }) {
        DeleteBodyContent()
    }
}

@Composable
fun DeleteBodyContent() {

    val mContext = LocalContext.current

    val nombreColeccion = "Contactos"
    val db = FirebaseFirestore.getInstance()

    var nombreBusqueda by remember { mutableStateOf("") }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(Purple200)
            .padding(top = 10.dp)
            .padding(start = 10.dp)
            .padding(end = 10.dp)
    ) {

        Image(
            painter = painterResource(id = R.drawable.agendapp1),
            contentDescription = "LogoAgenda",
            modifier = Modifier
                .padding(bottom = 24.dp)
                .size(width = 300.dp, height = 150.dp)
        )

        Text(
            text = "Eliminar contacto",
            style = MaterialTheme.typography.h2,
            color = Purple700
        )

        Spacer(modifier = Modifier.size(16.dp))

        OutlinedTextField(
            value = nombreBusqueda,
            onValueChange = { nombreBusqueda = it },
            label = {
                Text(
                    "Introduce el nombre",
                    color = Purple700,
                    style = MaterialTheme.typography.h3
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Purple700,
                unfocusedBorderColor = Purple500
            ),
            modifier = Modifier.fillMaxWidth(),
            singleLine = true,
        )


        Spacer(modifier = Modifier.size(16.dp))

        Button(

            onClick = {
                if (nombreBusqueda.isNotBlank()) {
                    db.collection(nombreColeccion)
                        .document(nombreBusqueda)
                        .delete()
                        .addOnSuccessListener {
                            mToast(mContext, "Contacto borrado")
                            nombreBusqueda = ""
                        }
                        .addOnFailureListener {
                            mToast(mContext, "No se ha podido borrar")
                            nombreBusqueda = ""
                        }
                } else {
                    mToast(mContext, "Introduce el nombre del contacto")
                }
            },

            colors = ButtonDefaults.buttonColors(
                backgroundColor = Purple500
            ),
            border = BorderStroke(2.dp, Purple300),
            shape = RoundedCornerShape(40),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .padding(vertical = 8.dp)
        )
        {

            Text(
                text = "Borrar",
                style = MaterialTheme.typography.button,
                color = White,
                modifier = Modifier.padding(start = 10.dp)
            )

        }
        Spacer(modifier = Modifier.size(5.dp))

    }
}

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}