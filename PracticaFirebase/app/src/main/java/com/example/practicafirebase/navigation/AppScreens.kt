package com.example.practicafirebase.navigation

sealed class AppScreens(val route: String) {

    object Inicio: AppScreens("Inicio")
    object Create: AppScreens("Create")
    object Read: AppScreens("Read")
    object Update: AppScreens("Update")
    object Delete: AppScreens("Delete")
    object ShowContacts: AppScreens("ShowContacts")

}
