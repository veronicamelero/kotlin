package com.example.practicafirebase.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.practicafirebase.R
import com.example.practicafirebase.navigation.AppScreens
import com.example.practicafirebase.ui.theme.*
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun Read(navController: NavHostController) {
    Scaffold(topBar = {
        TopAppBar(
            backgroundColor = Purple500
        ) {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Arrow Back",
                modifier = Modifier
                    .padding(end = 8.dp)
                    .align(CenterVertically)
                    .clickable {
                        navController.navigate(route = AppScreens.Inicio.route)

                    })
            Text(
                text = "Pantalla Inicio",
                style = MaterialTheme.typography.h2,
                color = Purple200
            )
        }

    }) {
        ReadBodyContent()
    }
}

@Composable
fun ReadBodyContent() {

    val nombreColeccion = "Contactos"
    val db = FirebaseFirestore.getInstance()

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(Purple200)
            .padding(top = 10.dp)
            .padding(start = 10.dp)
            .padding(end = 10.dp)
    ) {

        Image(
            painter = painterResource(id = R.drawable.agendapp1),
            contentDescription = "LogoAgenda",
            modifier = Modifier
                .padding(bottom = 24.dp)
                .size(width = 300.dp, height = 150.dp)
        )

        Text(
            text = "Búsca un contacto:",
            style = MaterialTheme.typography.h2,
            color = Purple700
        )

        Spacer(modifier = Modifier.size(16.dp))

        //DECLARAMOS LA VARIABLE QUE VA A RECOGER LOS DATOS DE LA CONSULTA CON EL ESTADO REMEMBER
        var datos by remember { mutableStateOf("") }

        val mContext = LocalContext.current

        var Nombre_busqueda by remember { mutableStateOf("") }

        var Nombre by remember { mutableStateOf("") }
        var Telefono by remember { mutableStateOf("") }
        var Email by remember { mutableStateOf("") }

        val fieldBusqueda = "Nombre"

        OutlinedTextField(
            value = Nombre_busqueda,
            onValueChange = { Nombre_busqueda = it },
            label = {
                Text(
                    "Introduce el nombre",
                    color = Purple700,
                    style = MaterialTheme.typography.h3
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Purple700,
                unfocusedBorderColor = Purple500
            ),

            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 8.dp, end = 8.dp),
            singleLine = true,
        )

        Spacer(modifier = Modifier.size(16.dp))

        Button(
            onClick = {
                if (Nombre_busqueda.isNotEmpty()){
                    // VACIAMOS VARIABLE AL DAR AL BOTON
                    datos = ""
                    Nombre = ""
                    Telefono = ""
                    Email = ""

                    // HACEMOS LA CONSULTA A LA COLECCION CON GET
                    db.collection(nombreColeccion)
                        .whereEqualTo(fieldBusqueda, Nombre_busqueda)
                        .get()

                        //SI SE CONECTA CORRECTAMENTE
                        // RECORRO TODOS LOS DATOS ENCONTRADOS EN LA COLECCIÓN Y LOS ALMACENO EN DATOS
                        .addOnSuccessListener { resultado ->
                            for (encontrado in resultado) {
                                //Para crear un HashMap con todos los datos
                                datos += "${encontrado.id}: ${encontrado.data}\n"

                                //Para crear un HashMap con todos los datos
                                Nombre += encontrado["Nombre"].toString()
                                Telefono += encontrado["Telefono"].toString()
                                Email += encontrado["Email"].toString()
                                //Log.i("DATOS:", datos)
                            }

                            if (datos.isEmpty()) {
                                mToast(mContext, "No existen datos")
                            }
                        }
                        //SI NO CONECTA CORRECTAMENTE
                        .addOnFailureListener { resultado ->
                            mToast(mContext, "Fallo en la conexion con la base de datos")
                        }
                } else {
                    mToast(mContext, "Introduce el nombre del contacto")
                }
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Purple500
            ),
            border = BorderStroke(2.dp, Purple300),
            shape = RoundedCornerShape(40),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .padding(vertical = 8.dp)
        )
        {

            Text(
                text = "Cargar Datos",
                style = MaterialTheme.typography.button,
                color = White,
                modifier = Modifier.padding(start = 10.dp)
            )
        }

        Spacer(modifier = Modifier.size(16.dp))

        // PINTAMOS EL RESULTADO DE LA CONSULTA A LA BASE DE DATOS

        if (datos.isNotEmpty()) {
            MostrarDatos(Nombre, Email, Telefono)
        }
    }

}

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}

@Composable
private fun MostrarDatos(Nombre: String, Email: String, Telefono: String) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp),
        elevation = 12.dp,
        shape = MaterialTheme.shapes.medium,
        backgroundColor = Purple100,
        border = BorderStroke(1.5.dp, Purple700)
    ) {
        Column() {
            Row(
                modifier = Modifier.padding(top = 16.dp, bottom = 8.dp)
            ) {
                Text(
                    text = "Nombre: ",
                    style = MaterialTheme.typography.button,
                    color = Purple700,
                    modifier = Modifier.padding(start = 10.dp)
                )

                Text(
                    text = Nombre,
                    style = MaterialTheme.typography.body1,
                    color = Purple700,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }
            Row(
                modifier = Modifier.padding(top = 8.dp, bottom = 8.dp)
            ) {
                Text(
                    text = "Teléfono: ",
                    style = MaterialTheme.typography.button,
                    color = Purple700,
                    modifier = Modifier.padding(start = 10.dp)
                )

                Text(
                    text = Telefono,
                    style = MaterialTheme.typography.body1,
                    color = Purple700,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }

            Row(
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 16.dp)
            ) {
                Text(
                    text = "Email: ",
                    style = MaterialTheme.typography.button,
                    color = Purple700,
                    modifier = Modifier.padding(start = 10.dp)
                )

                Text(
                    text = Email,
                    style = MaterialTheme.typography.body1,
                    color = Purple700,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }
        }
    }
}